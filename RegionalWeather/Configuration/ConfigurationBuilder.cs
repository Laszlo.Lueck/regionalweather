﻿#pragma warning disable
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using LanguageExt;
using RegionalWeather.Logging;
using Serilog;

#pragma warning restore

namespace RegionalWeather.Configuration;

public sealed record ConfigurationItems(string OwmApiKey, int RunsEvery, string PathToLocationsMap,
    string ElasticHostsAndPorts, string ElasticIndexName,
    int AirPollutionRunsEvery, string AirPollutionIndexName,
    string AirPollutionLocationsFile, string DatabaseHostName, string DatabaseName, string DbUserName,
    string DbPassword, string TableName);

public enum EnvEntries
{
    OwmApiKey,
    RunsEvery,
    PathToLocationsMap,
    ElasticHostsAndPorts,
    ElasticIndexName,
    AirPollutionRunsEvery,
    AirPollutionIndexName,
    AirPollutionLocationsFile,
    DatabaseHostName,
    DatabaseName,
    DbUserName,
    DbPassword,
    BackupTableName
}

public class ConfigurationBuilder
{
    private readonly IConfigurationFactory _configurationFactory;
    private readonly ILogger _logger;

    public ConfigurationBuilder(IConfigurationFactory configurationFactory)
    {
        _logger = Log.Logger.ForContext<ConfigurationBuilder>().ExtendedContext();
        _configurationFactory = configurationFactory;
    }

    public async Task<Option<ConfigurationItems>> GetConfigurationAsync()
    {
        var sw = Stopwatch.StartNew();
        try
        {
            return await Task.Run(GetConfiguration);
        }
        finally
        {
            sw.Stop();
            Console.WriteLine($"GetConfigurationAsync :: {sw.ElapsedMilliseconds} ms");
        }
    }


    private Option<ConfigurationItems> GetConfiguration()
    {
        _logger.Information("Try to read the configuration items from env vars");
        return
            from owmApiKey in _configurationFactory.ReadEnvironmentVariableString(EnvEntries.OwmApiKey)
            from runsEvery in _configurationFactory.ReadEnvironmentVariableInt(EnvEntries.RunsEvery)
            from pathToLocationsMap in _configurationFactory.ReadEnvironmentVariableString(EnvEntries
                .PathToLocationsMap)
            from elasticHostsAndPorts in _configurationFactory.ReadEnvironmentVariableString(EnvEntries
                .ElasticHostsAndPorts)
            from elasticIndexName in _configurationFactory.ReadEnvironmentVariableString(
                EnvEntries.ElasticIndexName)
            from airPollutionRunsEvery in _configurationFactory.ReadEnvironmentVariableInt(EnvEntries
                .AirPollutionRunsEvery)
            from airPollutionIndexName in _configurationFactory.ReadEnvironmentVariableString(EnvEntries
                .AirPollutionIndexName)
            from airPollutionLocationsFile in _configurationFactory.ReadEnvironmentVariableString(EnvEntries
                .AirPollutionLocationsFile)
            from databaseHostName in _configurationFactory.ReadEnvironmentVariableString(
                EnvEntries.DatabaseHostName)
            from databaseName in _configurationFactory.ReadEnvironmentVariableString(EnvEntries.DatabaseName)
            from dbUserName in _configurationFactory.ReadEnvironmentVariableString(EnvEntries.DbUserName)
            from dbPassword in _configurationFactory.ReadEnvironmentVariableString(EnvEntries.DbPassword)
            from backupTableName in _configurationFactory.ReadEnvironmentVariableString(EnvEntries.BackupTableName)
            select new ConfigurationItems(owmApiKey, runsEvery, pathToLocationsMap,
                elasticHostsAndPorts, elasticIndexName,
                airPollutionRunsEvery, airPollutionIndexName, airPollutionLocationsFile, databaseHostName,
                databaseName, dbUserName, dbPassword, backupTableName);
    }
}