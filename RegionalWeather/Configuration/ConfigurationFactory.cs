using System;
using LanguageExt;
using RegionalWeather.Utils;
using Serilog;

namespace RegionalWeather.Configuration;

public class ConfigurationFactory : IConfigurationFactory
{
    public Option<string> ReadEnvironmentVariableString(EnvEntries value, bool returnEmptyStringIfNoValue = false)
    {
        //Put some sugar here to tell why the container stops.
        return Environment
            .GetEnvironmentVariable(value.ToString())
            .ToOptionInternal();
    }

    public Option<int> ReadEnvironmentVariableInt(EnvEntries value)
    {
        return Environment
            .GetEnvironmentVariable(value.ToString())
            .ToOptionInternal()
            .Match(
            Some: variable => int.TryParse(variable, out var intVariable)
                ? intVariable
                : LogAndReturnNone<int>(value.ToString(), variable),
            None: () =>
            {
                Log.Warning($"No entry found for environment variable {value}");
                return Option<int>.None;
            }
        );
    }

    private static Option<T> LogAndReturnNone<T>(string envName, string value)
    {
        Log.Warning($"Cannot convert value {value} for env variable {envName}");
        return Option<T>.None;
    }

    public Option<bool> ReadEnvironmentVariableBool(EnvEntries value)
    {
        return Environment
            .GetEnvironmentVariable(value.ToString())
            .ToOptionInternal()
            .Match(
            Some: variable => bool.TryParse(variable, out var boolVariable)
                ? boolVariable
                : LogAndReturnNone<bool>(value.ToString(), variable),
            None: () =>
            {
                Log.Warning($"No entry found for environment variable {value}");
                return Option<bool>.None;
            }
        );
    }
}