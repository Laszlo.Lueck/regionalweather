﻿using System.Runtime.CompilerServices;
using Serilog;
using Serilog.Core;
using Serilog.Enrichers.ShortTypeName;
using Serilog.Sinks.SystemConsole.Themes;

namespace RegionalWeather.Logging;

public static class LoggerExtensions
{
    public static ILogger ExtendedContext(this ILogger logger,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0
    ) {
        return logger
            .ForContext("MemberName", memberName)
            .ForContext("FilePath", sourceFilePath)
            .ForContext("LineNumber", sourceLineNumber);
    }
}
    
internal static class BuildLoggerConfiguration
{
    public static Logger Build()
    {
        return new LoggerConfiguration()
            .Enrich.WithShortTypeName()
            .MinimumLevel.Information()
            .WriteTo.Console(theme: AnsiConsoleTheme.Code,
                outputTemplate:
                "[{Timestamp:yyy-MM-dd HH:mm:ss} {Level:u3} {ShortTypeName}] {Message:lj}{NewLine}{Exception}")
            .CreateLogger();
    }
}