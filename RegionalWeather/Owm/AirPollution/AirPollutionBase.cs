﻿using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace RegionalWeather.Owm.AirPollution;

public class AirPollutionBase : OwmBase
{

    public override string AsJson()
    {
        return JsonSerializer.Serialize(this);
    }

    [JsonPropertyName("coord")] public Coord Coord { get; set; }

    [JsonPropertyName("list")] public List<List> List { get; set; }

    [JsonPropertyName("name")] public string LocationName { get; set; }
}