﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace RegionalWeather.Owm;

public class OwmBase
{
    [JsonPropertyName("guid")]
    public Guid Guid { get; set; } 
        
    [JsonPropertyName("readTime")]
    public DateTime ReadTime { get; set; }

    public virtual string AsJson()
    {
        return JsonSerializer.Serialize(this);
    }

}