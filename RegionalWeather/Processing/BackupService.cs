﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Npgsql;
using RegionalWeather.Logging;
using RegionalWeather.Owm;
using RegionalWeather.Repositories;
using Serilog;

namespace RegionalWeather.Processing;

public class BackupService
{
    private readonly IBaseRepository _backupDataRepository;
    private readonly NpgsqlConnection _databaseConnection;
    private readonly ILogger _logger;

    public BackupService(NpgsqlConnection dbConnection)
    {
        _backupDataRepository = new BackupDataRepository(dbConnection);
        _databaseConnection = dbConnection;
        _logger = Log.Logger.ForContext<BackupService>().ExtendedContext().ExtendedContext();
    }

    public async Task BackupData<T>(IEnumerable<T> source, DataType dataType, string tableName) where T : OwmBase
    {
        _logger.Information("create a database transaction");
        var sw = Stopwatch.StartNew();
        await _databaseConnection.OpenAsync();
        var transaction = await _databaseConnection.BeginTransactionAsync();
        try
        {
            var tmpTableName = $"tmpTable_{DateTime.Now.ToFileTime().ToString()}";
            _logger.Information("temporary table name is {TempTableName}", tmpTableName);
            _logger.Information("convert data to dao");
            var daos = source.Select(item =>
            {
                var data = JsonSerializer.Serialize(item);
                return new DataTo(item.Guid, item.ReadTime, data, dataType);
            });
                
            _logger.Information("create temporary table");
            await _backupDataRepository.CreateTempTable(tmpTableName).ConfigureAwait(false);
            _logger.Information("create main table if not exists");
            await _backupDataRepository.CreateMainTable(tableName).ConfigureAwait(false);
            _logger.Information("transmit data");
            await _backupDataRepository.UpsertMany(tmpTableName, daos);
            _logger.Information("move data from temporary table to main");
            await _backupDataRepository.TempTableToMain(tmpTableName, tableName);
            _logger.Information("commit transaction");
            await transaction.CommitAsync();
            _logger.Information("finished processing of {DataType}", dataType.ToString());
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            _logger.Error(e, "an error while processing data to database occured");
        }
        finally
        {
            sw.Stop();
            if (_databaseConnection.State == ConnectionState.Open)
                await _databaseConnection.CloseAsync();
            _logger.Information("process time {Elapsed} ms", sw.ElapsedMilliseconds);
        }
    }
}