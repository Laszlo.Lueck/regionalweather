﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LanguageExt;
using RegionalWeather.Configuration;
using RegionalWeather.Elastic;
using RegionalWeather.FileRead;
using RegionalWeather.Logging;
using RegionalWeather.Owm.AirPollution;
using RegionalWeather.Repositories;
using RegionalWeather.Transport.Elastic;
using RegionalWeather.Transport.Owm;
using RegionalWeather.Utils;
using Serilog;
using static RegionalWeather.Processing.ProcessingHelper;

namespace RegionalWeather.Processing;

public class ProcessingBaseAirPollutionImpl
{
    private readonly IElasticConnection _elasticConnection;
    private readonly ILocationFileReader _locationFileReader;
    private readonly IOwmApiReader _owmApiReader;
    private readonly IProcessingUtils _processingUtils;
    private readonly IOwmToElasticDocumentConverter<AirPollutionBase> _owmToElasticDocumentConverter;
    private readonly ILogger _logger;

    public ProcessingBaseAirPollutionImpl(IElasticConnection elasticConnection,
        ILocationFileReader locationFileReader, IProcessingUtils processingUtils,
        IOwmApiReader owmApiReader, IOwmToElasticDocumentConverter<AirPollutionBase> owmToElasticDocumentConverter)
    {
        _elasticConnection = elasticConnection;
        _locationFileReader = locationFileReader;
        _owmApiReader = owmApiReader;
        _processingUtils = processingUtils;
        _owmToElasticDocumentConverter = owmToElasticDocumentConverter;
        _logger = Log.Logger.ForContext<ProcessingBaseAirPollutionImpl>().ExtendedContext();
        _logger.Information("Begin with etl process of air pollution information for locations");
    }

    public async Task Process(ConfigurationItems configuration)
    {
        var sw = Stopwatch.StartNew();
        try
        {
            var locationsList =
                await GetLocations(_locationFileReader, configuration.AirPollutionLocationsFile);
            
            _logger.Information("read the list of locations with {LocationCount} entries", locationsList.Count);

            var splitLocationList = SplitLocations(locationsList);
            
            var url = $"https://api.openweathermap.org/data/2.5/air_pollution?{{0}}&appid={configuration.OwmApiKey}";

            var rootTasks = DownloadData(splitLocationList, url, _owmApiReader, _logger);

            var rootOptions = await rootTasks
                .SequenceParallel(10)
                .Map(list => list.Somes());

            var readTime = DateTime.Now;
            _logger.Information("define document timestamp for elastic is {Elapsed}", readTime);


            var toElastic = await rootOptions
                .SequenceParallel(d => CalculateResult(d, readTime, DataType.AirPollution), 10)
                .Map(d => d.Somes())
                .MapAsync(async d =>
                {
                    var cp = d
                        .Map(element => (AirPollutionBase) element)
                        .ToList();
                    await _processingUtils.WriteDataToDatabase(cp, DataType.AirPollution, configuration.TableName);
                    return cp;
                });

            var elasticDocs = await toElastic
                .SequenceParallel(rootDoc => _owmToElasticDocumentConverter.ConvertAsync(rootDoc))
                .Map(list => list.Somes());
            
            var indexName = _elasticConnection.BuildIndexName(configuration.AirPollutionIndexName, readTime);
            _logger.Information("write air pollution data to index {IndexName}", indexName);
            if (!await _elasticConnection.IndexExistsAsync(indexName))
            {
                await _elasticConnection.CreateIndexAsync<AirPollutionDocument>(indexName);
                await _elasticConnection.RefreshIndexAsync(indexName);
                await _elasticConnection.FlushIndexAsync(indexName);
            }

            await _elasticConnection.BulkWriteDocumentsAsync(elasticDocs, indexName);
        }
        finally
        {
            sw.Stop();
            _logger.Information("Processed {MethodName} in {ElapsedMs:000} ms",
                "ProcessingBaseAirPollutionImpl.Execute",
                sw.ElapsedMilliseconds);
        }
    }
}