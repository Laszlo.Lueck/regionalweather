﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LanguageExt;
using RegionalWeather.Configuration;
using RegionalWeather.Elastic;
using RegionalWeather.FileRead;
using RegionalWeather.Logging;
using RegionalWeather.Owm.CurrentWeather;
using RegionalWeather.Repositories;
using RegionalWeather.Transport.Elastic;
using RegionalWeather.Transport.Owm;
using RegionalWeather.Utils;
using Serilog;
using static RegionalWeather.Processing.ProcessingHelper;

namespace RegionalWeather.Processing;

public class ProcessingBaseCurrentWeatherImpl
{
    private readonly IElasticConnection _elasticConnection;
    private readonly ILocationFileReader _locationFileReader;
    private readonly IOwmApiReader _owmApiReader;
    private readonly IProcessingUtils _processingUtils;
    private readonly IOwmToElasticDocumentConverter<CurrentWeatherBase> _owmToElasticDocumentConverter;
    private readonly ILogger _logger;

    public ProcessingBaseCurrentWeatherImpl(IElasticConnection elasticConnection,
        ILocationFileReader locationFileReader, IOwmApiReader owmApiReader, IProcessingUtils processingUtils,
        IOwmToElasticDocumentConverter<CurrentWeatherBase> owmToElasticDocumentConverter)
    {
        _elasticConnection = elasticConnection;
        _locationFileReader = locationFileReader;
        _owmApiReader = owmApiReader;
        _processingUtils = processingUtils;
        _owmToElasticDocumentConverter = owmToElasticDocumentConverter;
        _logger = Log.Logger.ForContext<ProcessingBaseCurrentWeatherImpl>().ExtendedContext();
        _logger.Information("Begin with etl process of weather information for locations");
    }

    public async Task Process(ConfigurationItems configuration)
    {
        var sw = Stopwatch.StartNew();
        try
        {
            _logger.Information("try to read weather information");
            var locationList =
                await GetLocations(_locationFileReader, configuration.PathToLocationsMap);
            
            _logger.Information("read the list of locations with {Locations} entries", locationList.Count);

            var splitLocationList = SplitLocations(locationList);

            var url =
                $"https://api.openweathermap.org/data/2.5/weather?{{0}}&APPID={configuration.OwmApiKey}&units=metric";

            var rootTasksOption = DownloadData(splitLocationList, url, _owmApiReader, _logger);

            var rootOptions = await rootTasksOption
                .SequenceParallel(10)
                .Map(d => d.Somes());


            var readTime = DateTime.Now;
            _logger.Information("define document timestamp for elastic is {ReadTime}", readTime);

            var toElastic = await rootOptions
                .SequenceParallel(d => CalculateResult(d, readTime, DataType.RegionalWeather), 10)
                .Map(d => d.Somes())
                .MapAsync(async d =>
                {
                    var cp = d
                        .Map(element => (CurrentWeatherBase)element)
                        .ToList();
                    await _processingUtils.WriteDataToDatabase(cp, DataType.RegionalWeather, configuration.TableName);
                    return cp;
                });


            var elasticDocs = await toElastic
                .Map(rootDoc => _owmToElasticDocumentConverter.ConvertAsync(rootDoc))
                .SequenceParallel(10)
                .Map(list => list.Somes());

            var indexName = _elasticConnection.BuildIndexName(configuration.ElasticIndexName, readTime);
            _logger.Information("write weather data to index {IndexName}", indexName);

            if (!await _elasticConnection.IndexExistsAsync(indexName))
            {
                await _elasticConnection.CreateIndexAsync<WeatherLocationDocument>(indexName);
                await _elasticConnection.RefreshIndexAsync(indexName);
                await _elasticConnection.FlushIndexAsync(indexName);
            }

            await _elasticConnection.BulkWriteDocumentsAsync(elasticDocs, indexName);
        }
        finally
        {
            sw.Stop();
            _logger.Information("Processed {MethodName} in {ElapsedMs:000} ms",
                "ProcessingBaseCurrentWeatherImpl.Execute",
                sw.ElapsedMilliseconds);
        }
    }
}