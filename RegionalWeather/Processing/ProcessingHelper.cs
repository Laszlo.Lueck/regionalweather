using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LanguageExt;
using RegionalWeather.FileRead;
using RegionalWeather.Owm;
using RegionalWeather.Owm.AirPollution;
using RegionalWeather.Owm.CurrentWeather;
using RegionalWeather.Repositories;
using RegionalWeather.Transport.Owm;
using Serilog;

namespace RegionalWeather.Processing;

public static class ProcessingHelper
{
    public static readonly Func<ILocationFileReader, string, Task<List<string>>> GetLocations =
        (locationFileReader, path) => locationFileReader.ReadLocationsAsync(path).IfNoneAsync(new List<string>());

    public static readonly Func<List<string>, IEnumerable<(string Parameter, string LocationName)>> SplitLocations =
        list => list.Map(element =>
        {
            var intermediate = element.Split(";");
            return (intermediate[0], intermediate[1]);
        });

    public static readonly
        Func<IEnumerable<(string Parameter, string LocationName)>, string, IOwmApiReader, ILogger,
            IEnumerable<Task<Option<(string LocationName, string Result)>>>> DownloadData =
            (parameters, url, ownApiReader, logger) =>
                parameters.Map(async parameter =>
                {
                    logger.Information("get air pollution information for configured {Location}",
                        parameter.LocationName);
                    var compUrl = string.Format(url, parameter.Parameter);
                    var resultOpt = await ownApiReader.ReadDataFromLocationAsync(compUrl, parameter.LocationName);
                    return resultOpt.Map(result => (parameter.LocationName, result));
                });

    public static async Task<Option<OwmBase>> CalculateResult((string, string) tuple,
        DateTime readTime, DataType dataType)
    {
        (string LocationName, string Result) tpl = tuple;
        Option<OwmBase> convertedBaseOpt = dataType switch
        {
            DataType.AirPollution => await ConvertToOwmBaseForAirPollution(tpl.Result, tpl.LocationName, readTime),
            DataType.RegionalWeather => await ConvertToOwmBaseForCurrentWeather(tpl.Result, tpl.LocationName, readTime),
            _ => throw new ArgumentOutOfRangeException(nameof(dataType))
        };

        return convertedBaseOpt;

        static async Task<Option<OwmBase>> ConvertToOwmBaseForAirPollution(string result, string locationName, DateTime readTime)
        {
            var baseResult = await Utils.Utils.DeserializeObjectAsync<AirPollutionBase>(result);
            return ConvertToSpecificAirPollution(baseResult, locationName, readTime);
        }

        static async Task<Option<OwmBase>> ConvertToOwmBaseForCurrentWeather(string result, string locationName,
            DateTime readTime)
        {
            var baseResult = await Utils.Utils.DeserializeObjectAsync<CurrentWeatherBase>(result);
            return ConvertToSpecificCurrentWeather(baseResult, locationName, readTime);
        }

        static Option<OwmBase> ConvertToSpecificCurrentWeather(Option<CurrentWeatherBase> convertedBaseOpt, string locationName, DateTime readTime)
        {
            return convertedBaseOpt.Map(convertedBase =>
            {
                static CurrentWeatherBase AddLocationNameToCurrentWeather
                    (CurrentWeatherBase cwBase, string locationName)
                {
                    cwBase.Name = locationName;
                    return cwBase;
                }

                OwmBase type = AddLocationNameToCurrentWeather(convertedBase, locationName);

                type.ReadTime = readTime;
                type.Guid = Guid.NewGuid();
                return type;
            });

        }

        static Option<OwmBase> ConvertToSpecificAirPollution(Option<AirPollutionBase> convertedBaseOpt, string locationName, DateTime readTime)
        {
            return convertedBaseOpt.Map(convertedBase =>
            {


                static AirPollutionBase AddLocationNameToAirPollution(AirPollutionBase apBase, string locationName)
                {
                    apBase.LocationName = locationName;
                    return apBase;
                }

                OwmBase type = AddLocationNameToAirPollution(convertedBase, locationName);

                type.ReadTime = readTime;
                type.Guid = Guid.NewGuid();
                return type;
            });
        }
    }
}