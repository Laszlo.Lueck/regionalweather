﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using RegionalWeather.Logging;
using RegionalWeather.Owm;
using RegionalWeather.Repositories;
using Serilog;

namespace RegionalWeather.Processing;

public interface IProcessingUtils
{
    public Task WriteDataToDatabase<T>(IEnumerable<T> dataList, DataType dataType, string tableName) where T : OwmBase;
}


public class ProcessingUtils : IProcessingUtils
{
    private readonly ILogger _logger;
    private readonly BackupService _backupService;

    public ProcessingUtils(BackupService backupService)
    {
        _backupService = backupService;
        _logger = Log.Logger.ForContext<ProcessingUtils>().ExtendedContext();
    }

    public async Task WriteDataToDatabase<T>(IEnumerable<T> dataList, DataType dataType, string tableName) where T : OwmBase
    {
        var sw = Stopwatch.StartNew();
        try
        {
            await _backupService.BackupData(dataList, dataType, tableName);
        }
        finally
        {
            sw.Stop();
            _logger.Information("Processed  in {ElapsedMs:000} ms", sw.ElapsedMilliseconds);
        }
    }
}