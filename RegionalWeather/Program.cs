﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using RegionalWeather.Configuration;
using RegionalWeather.Logging;
using RegionalWeather.Scheduler;
using Serilog;

namespace RegionalWeather;

class Program
{
    protected Program()
    {
    }

    private static ILogger _logger;

    static async Task Main(string[] args)
    {
           
        Log.Logger = BuildLoggerConfiguration.Build();

        _logger = Log.Logger.ForContext<Program>().ExtendedContext();
        _logger.Information("starting app");
        _logger.Information("current application version {ApplicationVersion}", Assembly.GetExecutingAssembly().GetName().Version);
            
            
        IConfigurationFactory configurationFactory = new ConfigurationFactory();

        var startupObjectOpt = await new ConfigurationBuilder(configurationFactory).GetConfigurationAsync();

        var mainTask = startupObjectOpt.Map(configuration =>
        {
            var sw = Stopwatch.StartNew();
            _logger.Information("Build up the scheduler");
            try
            {
                Task.Run(async () =>
                {
                    ISchedulerFactory currentWeatherSchedulerFactory =
                        new CustomSchedulerFactory<CurrentWeatherSchedulerJob>("currentWeatherJob",
                            "currentWeatherGroup", "currentWeatherTrigger", 10, configuration.RunsEvery,
                            configuration);
                    ISchedulerFactory airPollutionSchedulerFactory =
                        new CustomSchedulerFactory<AirPollutionSchedulerJob>("airPollutionJob",
                            "airPollutionGroup",
                            "airPollutionTrigger", 15, configuration.AirPollutionRunsEvery, configuration);
                    await currentWeatherSchedulerFactory.RunScheduler();
                    await airPollutionSchedulerFactory.RunScheduler();
                    _logger.Information("App is in running state!");
                });
                return Task.Delay(-1);
            }
            finally
            {
                sw.Stop();
                _logger.Information("Processed {MethodName} in {ElapsedMs:000} ms", "Main",
                    sw.ElapsedMilliseconds);
            }
        }).IfNone(() => Task.CompletedTask);


        await Task.WhenAll(mainTask);
        Environment.Exit(1);
    }
}