﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace RegionalWeather.Repositories;

public class BackupDataRepository : IBaseRepository
{
    private readonly NpgsqlConnection _connection;
    private readonly SemaphoreSlim _semaphoreSlim;

    public BackupDataRepository(NpgsqlConnection connection)
    {
        _connection = connection;
        _semaphoreSlim = new SemaphoreSlim(1, 90);
    }

    public async Task CreateTempTable(string tableName)
    {
        var stmt =
            $"CREATE TEMP TABLE IF NOT EXISTS {tableName} (id uuid, createtime timestamp, data json, datatype text) ON COMMIT DROP;";

        var cmd = new NpgsqlCommand(stmt, _connection);
        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
    }

    public async Task<int> CreateMainTable(string tableName)
    {
        var stmt =
            $"CREATE TABLE IF NOT EXISTS {tableName} (id UUID NOT NULL CONSTRAINT {tableName}_pk PRIMARY KEY, createtime TIMESTAMP, data JSON, datatype TEXT);";
        var cmd = new NpgsqlCommand(stmt, _connection);
        return await cmd.ExecuteNonQueryAsync();
    }

    public async Task TempTableToMain(string tempTableName, string mainTableName)
    {
        var stmt = $"INSERT INTO {mainTableName} SELECT * FROM {tempTableName} ON CONFLICT DO NOTHING;";
        var cmd = new NpgsqlCommand(stmt, _connection);
        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
    }

    public async Task UpsertMany(string tableName, IEnumerable<DataTo> dataTos)
    {
        var command = $"COPY {tableName} (id, createtime, data, datatype) FROM STDIN (FORMAT BINARY)";
        await _semaphoreSlim.WaitAsync().ConfigureAwait(false);
        var writer = _connection.BeginBinaryImport(command);

        var tasks = dataTos.Select(async dataTo =>
        {
            await writer.StartRowAsync().ConfigureAwait(false);
            await writer.WriteAsync(dataTo.guid, NpgsqlDbType.Uuid).ConfigureAwait(false);
            await writer.WriteAsync(dataTo.readTime, NpgsqlDbType.Timestamp).ConfigureAwait(false);
            await writer.WriteAsync(dataTo.data, NpgsqlDbType.Json).ConfigureAwait(false);
            await writer.WriteAsync(dataTo.dataType.ToString(), NpgsqlDbType.Text).ConfigureAwait(false);
        });
        await Task.WhenAll(tasks).ConfigureAwait(false);
        await writer.CompleteAsync().ConfigureAwait(false);
        await writer.CloseAsync().ConfigureAwait(false);
        await writer.DisposeAsync().ConfigureAwait(false);
        _semaphoreSlim.Release();
    }
}