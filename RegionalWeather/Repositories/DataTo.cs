﻿using System;

namespace RegionalWeather.Repositories;

public enum DataType
{
    RegionalWeather,
    AirPollution
}
    
public class DataTo
{
    public readonly Guid guid;
    public readonly DateTime readTime;
    public readonly string data;
    public readonly DataType dataType;

    public DataTo(Guid guid, DateTime readTime, string data, DataType dataType)
    {
        this.guid = guid;
        this.readTime = readTime;
        this.data = data;
        this.dataType = dataType;
    }
        
}