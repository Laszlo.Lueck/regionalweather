﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RegionalWeather.Repositories;

public interface IBaseRepository
{

    Task UpsertMany(string tableName, IEnumerable<DataTo> dataTos);

    Task TempTableToMain(string tempTableName, string mainTableName);

    Task CreateTempTable(string tableName);

    Task<int> CreateMainTable(string tableName);

}