﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using LanguageExt;
using Quartz;
using RegionalWeather.Configuration;
using RegionalWeather.Elastic;
using RegionalWeather.FileRead;
using RegionalWeather.Logging;
using RegionalWeather.Owm.AirPollution;
using RegionalWeather.Processing;
using RegionalWeather.Transport.Elastic;
using RegionalWeather.Transport.Owm;
using RegionalWeather.Transport.PostgreSql;
using Serilog;

namespace RegionalWeather.Scheduler;

public class AirPollutionSchedulerJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        var sw = Stopwatch.StartNew();
        var configuration = (ConfigurationItems) context.JobDetail.JobDataMap["configuration"];
        var logger = Log.Logger.ForContext<AirPollutionSchedulerJob>().ExtendedContext();
        try
        {
            await Task.Run(async () =>
            {
                logger.Information("Use the following parameters for this job");
                logger.Information("Runs every {RunsEvery} s", configuration.AirPollutionRunsEvery);
                logger.Information("Path to Locations file: {Locations}", configuration.AirPollutionLocationsFile);
                logger.Information("Write to Elastic index {IndexName}", configuration.AirPollutionIndexName);
                logger.Information("ElasticSearch: {ElasticHost}", configuration.ElasticHostsAndPorts);

                IElasticConnection elasticConnection =
                    new ElasticConnectionBuilder().Build(configuration);
                ILocationFileReader locationReader = new LocationFileReaderImpl();

                var dbConnection = new DbConnectionService().BuildConnection(configuration.DatabaseHostName,
                    configuration.DatabaseName, configuration.DbUserName, configuration.DbPassword,
                    Option<IEnumerable<string>>.None);
                var backupService = new BackupService(dbConnection);

                IProcessingUtils processingUtils = new ProcessingUtils(backupService);
                IOwmApiReader owmApiReader = new OwmApiReader();
                IOwmToElasticDocumentConverter<AirPollutionBase> owmConverter =
                    new AirPollutionToElasticDocumentConverter();

                var processor = new ProcessingBaseAirPollutionImpl(elasticConnection, locationReader,
                    processingUtils,
                    owmApiReader, owmConverter);


                await processor.Process(configuration);
            });
        }
        finally
        {
            sw.Stop();
            logger.Information("Processed {MethodName} in {ElapsedMs:000} ms", "AirPollutionSchedulerJob.Execute",
                sw.ElapsedMilliseconds);
        }
    }
}