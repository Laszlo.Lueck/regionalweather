﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using LanguageExt;
using RegionalWeather.Logging;
using Serilog;

namespace RegionalWeather.Transport.Owm;

public interface IOwmApiReader
{
    public Task<Option<string>> ReadDataFromLocationAsync(string url, string location);
}
    
    
public class OwmApiReader : IOwmApiReader
{
    private readonly ILogger _logger;

    public OwmApiReader()
    {
        _logger = Log.Logger.ForContext<OwmApiReader>().ExtendedContext();
    }

    public async Task<Option<string>> ReadDataFromLocationAsync(string url, string location)
    {
        var sw = Stopwatch.StartNew();

        try
        {
            var req = new HttpClient();
            var ret = await req.GetStringAsync(url);
            return ret;
        }
        catch (Exception exception)
        {
            _logger.Error(exception, $"Error while getting weather information for url {url}");
            return await Task.Run(() => Option<string>.None);
        }
        finally
        {
            sw.Stop();
            _logger.Information("Processed {MethodName} in {ElapsedMs:000} ms for {Location}", "ReadDataFromLocationAsync",
                sw.ElapsedMilliseconds, location);
        }
    } 
        
}