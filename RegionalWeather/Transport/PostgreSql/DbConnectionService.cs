﻿using System.Collections.Generic;
using LanguageExt;
using Npgsql;
using RegionalWeather.Logging;
using RegionalWeather.Utils;
using Serilog;

namespace RegionalWeather.Transport.PostgreSql;

public class DbConnectionService
{
    private readonly ILogger _logger;

    public DbConnectionService()
    {
        _logger = Log.Logger.ForContext<DbConnectionService>().ExtendedContext();
    }

    public NpgsqlConnection BuildConnection(string hostName, string databaseName, string userName, string password,
        Option<IEnumerable<string>> additionalParameters)
    {
        var additional = additionalParameters.Map(t => t.Join(";")).IfNone("");
        _logger.Information("Try to establish a connection to {DatabaseHost}", hostName);
        var connectionString =
            $"Host={hostName};Database={databaseName};Username={userName};Password={password};" + additional;
        return new NpgsqlConnection(connectionString);
    }
}