﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using LanguageExt;
using RegionalWeather.Elastic;
using RegionalWeather.Owm;
using Serilog;

namespace RegionalWeather.Utils;

public static class Utils
{
    public static Option<TIn> ToOptionInternal<TIn>(this TIn nullable)
    {
        return nullable;
    }

    public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
    {
        foreach (var item in source)
        {
            action.Invoke(item);
        }
    }

    public static async IAsyncEnumerable<Option<ElasticDocument>> ConvertToElasticDocuments<T>(List<T> toElastic,
        IOwmToElasticDocumentConverter<T> owmToElasticDocumentConverter) where T : OwmBase
    {
        foreach (var document in toElastic)
        {
            yield return await owmToElasticDocumentConverter.ConvertAsync(document);
        }
    }


    private static readonly ILogger Logger = Log.Logger.ForContext(typeof(Utils));

    public static string Join([NotNull] this IEnumerable<string> source, string separator) =>
        string.Join(separator, source);

    public static async Task<Option<T>> DeserializeObjectAsync<T>(string data)
    {
        var sw = Stopwatch.StartNew();
        try
        {
            await using MemoryStream stream = new();
            var bt = Encoding.UTF8.GetBytes(data);
            await stream.WriteAsync(bt.AsMemory(0, bt.Length));
            stream.Position = 0;
            var retValue = await JsonSerializer.DeserializeAsync<T>(stream).AsTask();
            return retValue != null ? retValue : Option<T>.None;
        }
        catch (Exception exception)
        {
            Logger.Error(exception, "Error while converting line to object");
            return await Task.Run(() => Option<T>.None);
        }
        finally
        {
            sw.Stop();
            Logger.Information("Processed {MethodName} in {ElapsedMs:000} ms",
                $"DeserializeObjectAsync<{typeof(T)}>",
                sw.ElapsedMilliseconds);
        }
    }
}