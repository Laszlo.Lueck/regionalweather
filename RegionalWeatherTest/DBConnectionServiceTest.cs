﻿using System.Collections.Generic;
using FluentAssertions;
using LanguageExt;
using RegionalWeather.Transport.PostgreSql;
using Xunit;

namespace RegionalWeatherTest;

public class DbConnectionServiceTest
{
    [Fact(DisplayName = "Build a db connection and check the parameter")]
    public void Build_and_check_db_connection()
    {
        var dbConnectionService = new DbConnectionService();
        var connection = dbConnectionService.BuildConnection("a", "b", "c", "d",
            Option<IEnumerable<string>>.Some(new List<string> { "Port=1234" }));

        connection.Should().NotBeNull();
        connection.ConnectionString.Should().Contain("Host=a");
        "b".Should().Be(connection.Database);
        "c".Should().Be(connection.UserName);
        connection.ConnectionString.Should().Contain("Port=1234");
    }
        
}