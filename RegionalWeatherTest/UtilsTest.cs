using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnsafeValueAccess;
using RegionalWeather.Utils;
using Xunit;

namespace RegionalWeatherTest;

public class UtilsTest
{
    [Fact(DisplayName = "Test the extension helper join method")]
    public void Test_join_method()
    {
        var list = new List<string> { "a", "b", "c" };

        var str = list.Join(",");

        "a,b,c".Should().Be(str);
    }

    [Fact(DisplayName = "Test the deserialization of an object")]
    public async Task Test_Deserialize_from_utils()
    {
        var str = "{\"Id\":1, \"Name\":\"FollowTheWhiteRabbit\"}";

        var result = await Utils.DeserializeObjectAsync<TestObject>(str);

        result.Should<Option<TestObject>>().NotBe(Option<TestObject>.None);
        var concreteValue = result.ValueUnsafe();
        concreteValue.Id.Should().Be(1);
        concreteValue.Name.Should().Be("FollowTheWhiteRabbit");
    }

    [Fact(DisplayName = "Test the deserialization of an object with error")]
    public async Task Test_Deserialize_from_utils_with_error()
    {
        var str = "{\"NoId\":7, \"Name\":1}";

        var result = await Utils.DeserializeObjectAsync<TestObject>(str);
        result.Should<Option<TestObject>>().Be(Option<TestObject>.None);

    }
        
    private class TestObject
    {
#pragma warning disable S1144
        public TestObject(int id, string name)
        {
            Id = id;
            Name = name;
        }
#pragma warning restore

        public int Id { get; }
        public string Name { get; }
    }
}